---
title: "Qualif FAQ"
---
### General

#### How can I get help?

You can contact the support through the <ci-support@inria.fr> mail
address.

#### I have multiple projects to set up. Should I create multiple Jenkins projects in the portal or should I create a single Jenkins project with multiple jobs ?

**Short answer:** if these projects concern the same group of developers
and/or if they have dependencies with each other, then you may prefer to
create a single Jenkins project. In other cases you should create
multiple projects.

**Longer answer:** there are multiple criteria you need to take into
account:

-   privileges are granted in a per-project basis, not on a per-job
    basis. For example, you cannot have a public and a private job in
    the same Jenkins project.
-   Jenkins can handle dependencies between jobs (eg. a software that
    depends on libraries). Especially it can be configured trigger a new
    build if one dependency is updated.
-   slave nodes cannot be shared between projects. If you have to set up
    a very specific configuration for your different projects, then you
    will need to clone thes slave nodes.

### Portal

#### What is the username to connect to the platform?

The username to log on the various platform is usually your email
address (for the portal, the jenkins websites and the cloudstack
front-end). To log on the virtual machine network, you have to use the
uid provided on the front-end <http://ci.inria.fr> when click on your
email address (upper-right) and "My account".

#### Why do I have to create a new account to log on the CI portal, while it would be simpler to log in with my INRIA iLDAP account?

Because all users are not INRIA users only, it is necessary for the CI
platform to have its own LDAP. Synchronisation with iLDAP would be
tricky. A single sign-on may be implemented in the future, but it's not
considered to be a priority.

#### I work at INRIA (or a partner lab) but my account is identified as a "Guest"

When creating your account, you must use the same e-mail address as the
one registered in the Inria LDAP directory.

If you do not know this address, you can make a query at this page:
<https://annuaire.inria.fr/>

### Jenkins

#### The list of available plugins is empty

In the plugin configuration page (**Manage Jenkins** -\> **Manage
Plugins**), go into the **Advanced** tab and click on the button **Check
now** at the bottom right of the page.

#### Display CTest results in Jenkins

CTest results can be displayed using the xUnit plugin and the
[jenkins-ctest-plugin](https://github.com/damienmg/jenkins-ctest-plugin).
To do so:

1\. Install the xUnit plugin in Jenkins

2\. Go to your execution slaves and get the
[jenkins-ctest-plugin](https://github.com/damienmg/jenkins-ctest-plugin):

`su ci -c "cd && git clone `[`https://github.com/damienmg/jenkins-ctest-plugin.git`](https://github.com/damienmg/jenkins-ctest-plugin.git)`"`

3\. Add a shell build step to your job in Jenkins to do the test and get
the XML results (if your CMake build directory is "build"):

`cd build`\
`/builds/jenkins-ctest-plugin/run-test-and-save.sh`

4\. Add a "Publish xUnit Results" post-build step to your job in
Jenkins and click "Add" / "Custom tools". Set the pattern to be
"build/CTestResults.xml" (again, if "build" is your CMake build
directory) and the custom stylesheet to
"/builds/jenkins-ctest-plugin/ctest-to-junit.xsl" (if /builds is the
ci user home as provided by the default templates).

#### I want to authorize anonymous to see my builds

Got to Manage Jenkins / Configure System / Authorization, set read for
'anonymous' (overall) in job's column.

#### I want to automatically run a job after a commit (using SVN SCM)

1.  Go to job configuration for <YOUR_JOB>, click on "Trigger builds
    remotely (e.g., from scripts)" (FR "Déclencher les builds à
    distance") and set the token to a value: <TOKEN>. Don't forget to
    save your configuration.
2.  Click on your username <USER> in the left top corner in Jenkins
    windows, next "Configure\..." et "Show API Token\...", copy the
    value of <APITOKEN>.
3.  Now you can use wget command in your post-commit script, you can try
    it in a shell with wget.

To add a post-commit script on gforge.inria.fr, you have to connect
using you SSH key to scm.gforge.inria.fr and put your lines in
/svnroot/<GFORGEPROJECT>/hooks/post-commit or
/gitroot/<GFORGEPROJECT>/<GFORGEPROJECT>.git/hooks/post-receive.

Replace <USER>, <APITOKEN>, <PROJECT> (the project unix name as provided
in the portal), <YOUR_JOB> and <TOKEN> with your personnal values:

-   With wget 1.11 or upper, use this:

` wget --auth-no-challenge --http-user=`<USER>` --http-password=`<APITOKEN>\
`      `[`http://ci.inria.fr/`](http://ci.inria.fr/)<PROJECT>`/job/`<YOUR_JOB>`/build?token=TOKEN`

-   With wget 1.10.x:

` wget `[`http://`](http://)<USER>`:`<APITOKEN>`@ci.inria.fr/`<PROJECT>`/job/`<YOUR_JOB>`/build?token=`<TOKEN>

#### I want to automatically run a job after a commit (using Git SCM)

If you are using GIT plugin on Jenkins, you have a simplified method to
run your job after commit :

1.  Go to job configuration for <YOUR_JOB>, click on "Poll SCM" (FR
    "Scrutation de l'outil de gestion de version") and set the value
    to: "\# Leave empty. We don't poll periodically, but need polling
    enabled to let HTTP trigger work". Don't forget to save your
    configuration.
2.  Now you can use wget command in your post-commit Git script, you can
    try it in a shell with wget.

To add a post-commit script on gforge.inria.fr, you have to connect
using you SSH key to scm.gforge.inria.fr and put the next wget command
in /gitroot/<GFORGEPROJECT>/<GFORGEPROJECT>.git/hooks/post-receive.

`wget --auth-no-challenge --no-check-certificate \`\
`       `[`http://ci.inria.fr/`](http://ci.inria.fr/)<PROJECT>`/git/notifyCommit?url=`[`git://scm.gforge.inria.fr/gitroot/`](git://scm.gforge.inria.fr/gitroot/)<GFORGEPROJECT>`/`<GFORGEPROJECT>`.git`

A **more complete script that support branches** is
[post-receive](../media:git-post-receive.sh "wikilink"). You just have
to adapt the first lines (the PROJECT variable and the GFORGEPROJECT
variable).

#### I want to grab file X from the Jenkins server / get the ip address of the Jenkins server

You can do lot of stuff using the groovy script. Go to "Manage
Jenkins" / "Script console" and you will be able to type scripts. For
instance, if you wish to execute the "hostname" command to grab the
server host name, do:

` def command = """hostname"""`\
` def proc = command.execute()`\
` proc.waitFor()`\
` println "stdout: ${proc.in.text}"`

#### I want to use subversion on a private project with public key authentication

1\. Create a ssh key pair by running « ssh-keygen »

` ssh-keygen`

Now the new key pair should be located in \~/.ssh/id\_rsa.pub (public
key) and \~/.ssh/id\_rsa (private key). On Windows these two files
should be located in %HOME\_DIR%\\.ssh

2\. You should have an account with read rights on your private SVN
repository, ideally (for security reasons) this account should be
dedicated to your jenkins instance and only have access to the project
you want to build. It is not recommended to use your personnal account.

3\. Install the public key (\~/.ssh/id\_rsa.pub) on your git repository
to allow the ci user to authenticate using his key (if you are using
INRIA's forge, ssh keys are configured on this page:
<https://gforge.inria.fr/account/editsshkeys.php>, wait 1 hour before
the keys is actually deployed on the Inria's forge).

4\. Configure your Jenkins job (in the web interface of Jenkins)

-   In the « Source Code Management » section, select « Subversion »
-   Fill the « Repository URL » box with the URL of your SVN repository
    (typically starting with svn+ssh://). Jenkins will report an error «
    No credential to try. Authentication failed (show details)

(Maybe you need to enter credential?) », this is normal, click on
"credentials" and then select "SSH public key authentication". Enter
the username of the ci account on the subversion repository and select
the SSH key (the \~/.ssh/id\_rsa file). Enter the passphrase if any.

-   Finish the configuration of your Jenkins job and save your changes.

#### I want to use a private GIT repository with public key authentication (this is the case for INRIA's forge)

1\. Install the GIT plugin into your Jenkins instance

2\. Install git on the build slave (Note: for windows slaves you should
install the « Git for Windows » from <http://msysgit.github.com/>)

3\. Create a ssh key pair on the slave by running « ssh-keygen » **AS THE
ci USER** (Note: for windows slave this command should be run in « Git
Bash » shell accessible in the Start menu).

` su ci -c ssh-keygen`

Now the new key pair should be located in \~/.ssh/id\_rsa.pub (public
key) and \~/.ssh/id\_rsa (private key). On Windows these two files
should be located in C:\\users\\ci\\.ssh

4\. Your ci user on the build slave needs to have his « author » and «
email » properties configured (even if you do not indend to do any
commit)

` git config --global user.name "CI"`\
` git config --global user.email ci@dummyuser.ci.inria.fr`

5\. The ssh host key of your repository needs to be recorded in the
*known\_hosts* file of the ci user. To do that, just open a ssh session
to the remote server and accept the server key. Thus if your repository
is git+ssh://scm.gforge.inria.fr/gitroot/\... then you will run:

` ssh scm.gforge.inria.fr`\
` `\
` The authenticity of host 'scm.gforge.inria.fr (131.254.249.52)' can't be established.`\
` RSA key fingerprint is 00:0c:90:8b:9d:b7:91:b8:ce:75:85:3b:c0:ee:73:07.`\
` Are you sure you want to continue connecting (yes/no)? yes`\
` Warning: Permanently added 'scm.gforge.inria.fr,131.254.249.52' (RSA) to the list of known hosts.`\
` Permission denied (publickey).`

6\. You should have an account with read rights on your private GIT
repository, ideally (for security reasons) this account should be
dedicated to your jenkins instance and only have access to the project
you want to build. It is not recommended to use your personnal account.

7\. Install the public key (\~/.ssh/id\_rsa.pub) on your git repository
to allow the ci user to authenticate using his key (if you are using
INRIA's forge, ssh keys are configured on this page:
<https://gforge.inria.fr/account/editsshkeys.php>)

8\. Configure your Jenkins job (in the web interface of Jenkins)

-   In the « Source Code Management » section, select « Git »
-   Fill the « Repository URL » box with the URL of your GIT repository
    (typically starting with git+ssh:// or <ssh://>). Jenkins will
    report an error « Failed to connect to repository », this is normal.
    Just ignore it and save your changes.

9\. (Windows slaves only) Configure the HOME environment variable in your
node:

-   Go to the configuration page of your node (menu Manage Jenkins -\>
    Manage Nodes -\> *your node* -\> Configure)
-   In the « Node Properties » section, tick the « Environment variables
    » box
-   Click on the « Add » button to add a key/value pair
-   Set the name to « HOME » and the value to « C:\\Users\\ci » (this is
    the directory containing the .ssh/ configuration subdirectory)

#### The GIT plugin freezes during the build (eg. when cloning the remote repository)

This is most likely due to an error in the configuration. The Jenkins
plugin filters the standard error output of the git command, thus you
are left with very little feedback.

Check the previous section of this Qualif\_FAQ to ensure that everything
is well configured.

#### I want to use the output of a job as the input of another job, how can I do that?

You can that by installing the [Copy Artifact
Plugin](http://wiki.jenkins-ci.org/display/JENKINS/Copy+Artifact+Plugin)
in your Jenkins instance, then add a post-build action to your first job
to archive the files to use as input on your second job. Finally, you
add a "copy artifacts from another project" step in your second job.

#### Jenkins cannot send emails.

Please check :

-   either 'sender email address' in the 'email notification'
    category for Jenkins versions \< 1.474,
-   or '' in the Jenkins Location category for newer versions.

It must be an email address from an inria domain (ex:
jenkins@ci.inria.fr).

#### Jenkins closes SVN connection.

    Caused by: org.tmatesoft.svn.core.SVNErrorMessage: svn: Sorry, this connection is closed.
         at org.tmatesoft.svn.core.SVNErrorMessage.create(SVNErrorMessage.java:101)
        at org.tmatesoft.svn.core.internal.io.svn.SVNSSHConnector2.open(SVNSSHConnector2.java:153)

It's a well-known issue in SVN plug-in 1.37, upgrading to 1.45 or 1.50
resolves issue.

### CloudStack

#### What is the domain name to log on the CloudStack website?

The domain name to log on the CloudStack website is ***ci/projectname***
where ***projectname*** is the unixname of the project created on the
continuous integration portal (http://ci.inria.fr)

#### How to open ports on the slaves?

In the CloudStack platfrom, go to the **Network** tab, select the
**Security Group** Views in the **Select view** combobox on the top.
Click on the **default** item and select the **Ingress Rule** You can
now open new ports by entering new rules.

#### How can I connect to Windows? The remote desktop is not working\...

First, make sure you have created a SSH tunnel (explained [here for
Linux/MacOS
X](../qualif_cloudstack_tutorial#linux.2fmacos-users "wikilink") and
[here for
Windows](../qualif_cloudstack_tutorial#windows-users "wikilink")).
Second, the default Remote Desktop Client is not working properly most
of the time in Mac OS X (maybe on some other systems too). Please try
another client such as [CoRD](http://cord.sourceforge.net)

#### Where to put big templates or ISOs for uploading in CloudStack ?

Temporarily you can put these big files on scm.gforge.inria.fr. See some
documentation here :
<http://siteadmin.gforge.inria.fr/Qualif_FAQ.html#SCM>

-   Copy file on GForge : scp <file>
    <username>@scm.gforge.inria.fr:/home/groups/<project>/incoming/<file>
-   In GForge go to *Documents* tab :
    <https://gforge.inria.fr/docman/?group_id=><projectid>
-   Submit a new document, choose option *uploaded file* and your <file>
    in the list below.
-   Now the URL to download your file from *CloudStack* is :
    <https://gforge.inria.fr/docman/view.php/><projectid>/<fileid>/<file>
-   Don't forget to put read access for anonymous to documents in tab
    *Administration*.

### Slaves

#### General

##### How is defined a slave hostname?

The hostname of a slave in the cloud infrastructure (thus, from the SSH
gateway) is : **projectname**-**slavename**.

##### Should I create one or multiple slaves ?

You will create multiple slaves if your software needs to support
multiple environments, like:

-   different operating sysems (Linux, Windows, \...)
-   different linux distributions (Ubuntu, Debian, Fedora, \...)
-   different x86 CPU (32 bits or 64 bits)

Thus you have to create one slave for each configuration you want to
support.

##### *ssh root@my-slave.ci* says it cannot find my-slave.ci. What is this .ci domain anyway ?

Indeed .ci is not a real internet domain name.

The procedure to connect to the slaves is a little cumbersome because
they lie in a private network (and it requires to open a tunnel through
the *ci-ssh.inria.fr* frontend).

To make it easy, we recommend to [set up a proxy command in your ssh
configuration
file](../qualif_slaves_access_tutorial#configure-a-ssh-proxy-command- "wikilink").
Once this configuration is done, *ssh root@my-slave.ci* should bring
you to the right place.

##### cat /proc/cpuinfo (or Windows) gives me information that are not consistent with the information seen on the Web Portal, what is right?

Actually, the service offer give twice more timeslots to a 2GHz virtual
machine that to a 1GHz virtual machine (so it is not a processor speed
per se). The displayed frequency about the CPU on the virtual machine is
the actual host CPU frequency (and not the allocated CPU timeslot).

##### My slave crashed. Even after a reboot I cannot connect to it. What can I do ?

Through the [CloudStack
interface](../qualif_cloudstack_tutorial "wikilink") you can have access
to the console of your slave and fix what is preventing it to boot.

Once on the CloudStack dashboard:

1.  go to the **Instances** tab
2.  select the slave you want to debug
3.  click on the **\>\_** button

Then a new window will open and give access to the console of your
slave, just like if your were sitting in front of it.

![](/doc-ci/img/CloudStack-Console.png "/doc-ci/img/CloudStack-Console.png")

#### Linux

##### What is the password for the templated Linux slaves?

The default root password is **password**.

The default password of ci (the user running the jenkins jobs) is
**ci**.

#### Windows

##### What is the password for the templated Windows slave?

The default administrator account created is **ci** with the password
**ci**

##### How to change a password via rdesktop

Do not hit Ctrl-Alt-Del (really!)

To change the user password, select: **Start** → **Windows Security** →
**Change Password**

##### How to set up a build in the MinGW/MSYS shell?

-   install the MinGW toolchain and the MSYS developer environment from
    <https://sourceforge.net/projects/mingw/files/Installer/mingw-get-inst/>
-   prepend these lines at the beginning of the build script in your
    jenkins project:

`#!c:\mingw\msys\1.0\bin\sh -login`\
`cd "/c/builds/workspace/$JOB_NAME"`
