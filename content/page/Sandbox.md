---
title: "Sandbox"
---
Introduction
------------

### Jenkins

Firstly, Continuous integration require to manage all the testing
processes. For example, one could want the testing processes to be
triggered by every commit on a SVN server, consisting in updating the
sources, recompiling what had changed, running a test suite, generating
and serving HTML report, and sending email if some tests failed. All
these kind of things are manage using a Continuous integration software.
The Continuous integration software used by Inria **Jenkins**. Jenkins
(like other Continous integration integration softwares) is organized in
a slave/master model. The master is the daemon running on the
master-machine, and is responsible to trigger the testing processes by
polling a source code repository or using cron-like rules or whatever,
aggregate the tests results, generate HTML report etc. Slave are the
client part running on slave-machines, where the tests are effectively
run. Typically, all the configuration (the commands that have to be
executed on the slaves update the sources, compile, run the tests, \...)
is done on the master, and several slaves arebe created for different
operating systems and achitectures.

### CloudStack

Secondly, Continuous integration require to create and manage the slave
machines, where all the testing processes are executed. A crucial point
in Continuous integration is allow testing on operating systems or
architectures differents from the one the developper work on.
Additionnlly, one could want to execute tests on a fresh installation,
or to dispose of pre-installed software. The cloud infrastruture used by
Inria is **CloudStack**.

### The Web portal

In the next section, you will learn how to use the **Web portal** to
create a project on the Inria Continuous intergration plateform, and to
use it to manage your project, reach your **Jenkins** and
**CloundStack** instances. A project will consist in one Jenkins master
instance, and multiple slave instances (20 at a maximum). All slaves are
virtual machines, that are private to each project, in the sense that a
slave can not be used in two different projects. A project can regroup
different developpers, and a developper can create or participate in
different projects. The Web portal provide you an interface to manage
multiple projects.

Getting started
---------------

### Creating a user account

Visit the [front page](https://ci.inria.fr) of the Web portal, sign up
(a valid email adress will be asked), and log in.

To log in, use the **email adress** as username.

![](/doc-ci/img/CI-Portal-SignUp-Login.png "/doc-ci/img/CI-Portal-SignUp-Login.png")

### The navigation bar

It is worth to note that, at the top of the each page, the navigation
bar give quick links to:

-   Sign up, Log in, Log out.
-   Dashboard.
-   Projects creation and list.
-   Users list.
-   User account configuration (names, email, password, SSH keys\...).

Note that your brower windows must be large enough to see all items of
the navigation bar.

![](/doc-ci/img/CI-Portal-Navigation-Bar.png "/doc-ci/img/CI-Portal-Navigation-Bar.png")

### Install SSH key

From the navigation bar, select *My account*. Copy, paste and add now
your SSH public key, so that it can be used later to connect to the
slaves machine you will create.

![](/doc-ci/img/CI-Portal-Ssh.png "/doc-ci/img/CI-Portal-Ssh.png")

### Create a new project

Go to the [Dashboard](https://ci.inria.fr/dashboard) page. From here,
you can create a new project, join an existing project, and manage the
project you own or participate in.

Click the green button *Host my project*:

![](/doc-ci/img/CI-Portal-Dashboard-New-Project.png "/doc-ci/img/CI-Portal-Dashboard-New-Project.png")

and fill the project creation form:

-   Shortname. It will be the identifiant (the Unix-name) of your
    project, use lower case only.
-   Fullname.
-   Description.
-   Visibility.
-   Software. Only Jenkins for now, but more Continuous integration
    software may be supported in the future.

![](/doc-ci/img/CI-Portal-Project-Creation.png "/doc-ci/img/CI-Portal-Project-Creation.png")

Once the form has been submitted, a Jenkins master instance will be
created for this project. You are now ready to create the slaves using
CloudStack and configure Jenkins.

### Manage the project

On the Dashboard, your new project should appear, with tree links.

![](/doc-ci/img/CI-Portal-Dashboard.png "/doc-ci/img/CI-Portal-Dashboard.png")

-   The
    ![](/doc-ci/img/CI-Portal-Jenkins-Button.png "fig:/doc-ci/img/CI-Portal-Jenkins-Button.png")
    button links to the **Jenkins master** instance web interface, the
    place where all the Jenkins configuration if performed. More
    precisely, the link points to the **production** version. Production
    and qualification concept are explained above.

<!-- -->

-   The
    ![](/doc-ci/img/CI-Portal-Slaves-Button.png "fig:/doc-ci/img/CI-Portal-Slaves-Button.png")
    button links to the **CloudStack platform**, where slaves for this
    project (and this project **only**) are created or configured.

<!-- -->

-   The
    ![](/doc-ci/img/CI-Portal-Project-Button.png "fig:/doc-ci/img/CI-Portal-Project-Button.png")
    button links to the **Project configuration**, a part of the Web
    portal.

**Take a moment to configure Jenkins by reading the [jenkins
tutorial](#jenkins-tutorial "wikilink") and create one or more slave as
describe in the [couldstack
tutorial](#cloudstack-tutorial "wikilink").**

Once done, come back and continue this tutorial by clicking the
![](/doc-ci/img/CI-Portal-Project-Button.png "fig:/doc-ci/img/CI-Portal-Project-Button.png")
button to open the configuration of your project.

1.  ![ Project
    abstract](/doc-ci/img/CI-Portal-Project-0-Abstract.png "fig: Project abstract")
2.  ![ Project
    users](/doc-ci/img/CI-Portal-Project-1-Users.png "fig: Project users")
3.  ![ Project
    Jenkins](/doc-ci/img/CI-Portal-Project-2-Jenkins.png "fig: Project Jenkins")
4.  ![ Project
    slaves](/doc-ci/img/CI-Portal-Project-3-Slaves.png "fig: Project slaves")
5.  ![ Project
    logs](/doc-ci/img/CI-Portal-Project-4-Logs.png "fig: Project logs")

The **Slaves tab** will display the slaves you created on CloudStack and
the informations required to connect the slave using ssh. You also have
the possibily to add new slave from here, without connecting the
CouldStack platform.

The **Jenkins tab** offer a powerfull way to securise the changes to
want to apply to your project Jenkins intance. Whether it is a change in
the Jenkins configuration (how sources are polled, how tests are run,
\...) or an upraged of Jenkins version, or an upgrade of a Jenkins
plugin version, all theses changes can be dangerous and make your
project Continuous integration crash. Instead of applyng changes
directly on the **production Jenkins**, one can test it on a
**qualification Jenkins**. Once the qualification Jenkins is proved to
be stable, one can decide to replace the production Jenkins with it.

Take a look at the **Abstract**, **Users**, and **Logs** tabs.

### Users and project lists

From the navigation bar, you can consult both the users and the projects
list. It permits to delete your user account, a project you own, or join
another project.
