---
title: "Using Jenkins with Gitlab"
---
Using Jenkins CI with a Gitlab Repository
=========================================

Introduction
------------

In this tutorial, we want to help the user to run the Jenkins Continuous
Integration Service along the Gitlab platform. At the end of the
configuration, your project will behave typically like :

-   runs automatically jobs on CI/Jenkins when a commit is pushed on the
    master branch in Gitlab
-   checkouts the changes in the sources of your Gitlab project
-   perform the build of the project (code generation, compilations,
    tests, code inspection, etc.)
-   sends a report about building to the Gitlab platform

There is **another** way to do that : using the Gitlab CI tool with
**gitlab-runners**. In this case, the gitlab platform will be
responsible for launching jobs on your VMs that your have to provide to
gitlab. In this tutorial, we will not speak about this point of view :
all the orchestration will be done by Jenkins CI

Prerequisites
-------------

-   we assume that you know how to configurate a project on Jenkins, we
    will speak about how to adapt the process to the Gitlab platform
-   we assume also that your have ever used the gitlab platform

Install plugins and configure plugins
-------------------------------------

-   To work with the Gitlab platform, you need to install the following
    Jenkins extensions
    1.  Git plugin
    2.  Gitlab plugin
-   Not that if you have not ever installed the Git plugin and you
    select Gitlab plugin, Jenkins will select automatically to install
    the Git plugin

Check on the page
*https://ci.inria.fr/\<nameOfTheProjectOnCI\>/pluginManager/installed*
if the plugins are already installed, if not you can install them on the
page
*https://ci.inria.fr/\<nameOfTheProjectOnCI\>/pluginManager/available*

### Gitlab plugin

-   To configure that plugin, we need to generate a Secret Token on the
    Gitlab platform, to enable Jenkins to use the web API of Gitlab

#### Generate a API Token

-   On Gitlab, click your account avatar, follow the sub-menu
    *Settings*, and select the tab *Access Tokens*

![](/doc-ci/img/apiToken.png "/doc-ci/img/apiToken.png")

-   Choose a name and an expiration date, select *api*, and click
    *Create Personal Access Token*
-   DO NOT FORGET TO SAVE the content of the field *Your New Personal
    Access Token* to remember later.

#### Jenkins Credentials

-   Go to *Credentials*, *System*, *Global credentials (unrestricted)* (
    or
    *https://ci.inria.fr/\<nameOfTheProjectOnCI\>/credentials/store/system/domain/\_/*
    ) and click *Add Credentials*
-   In the Field *Kind*, select *Gitlab API Token* and put the
    previously saved token, put *gitlab-1* for *ID* and access
    your\_login for *Description*, click *Ok*

#### Gitlab plugin Configuration

-   On Jenkins, Go to *Manage Jenkins*, *Configure System* ( or
    *https://ci.inria.fr/\<nameOfTheProjectOnCI\>/configure* )and Go to
    the *Gitlab* section; Configure like in the screenshot, selecting
    Gitlab API Token for credentials. Click on *Test Connection* to make
    a small test

![](/doc-ci/img/gitlab_plugin.png "/doc-ci/img/gitlab_plugin.png")

-   Save your config

Retrieving the sources
----------------------

### Public project

In this case, you do not need to have a special credential to retrieve
the sources of your project\
just add the
git@gitlab.inria.fr:\<userGitlab\>/\<nameOfTheProjectOnGitlab\>.git in
the Source Management section

### Private project

Before, with Gforge, we use to create the *Ghost* user on the Gforge
platform corresponding\
to the Jenkins user (see section 3.9 of the FAQ
[FAQ](https://wiki.inria.fr/ciportal/FAQ#I_want_to_use_a_private_GIT_repository_with_public_key_authentication_.28this_is_the_case_for_INRIA.27s_forge.29)).
To do the same thing, we will use the *deploy keys* feature in Gitlab :

-   first create a pair of public/private key on a slave (logged as ci )

`cd /builds && ssh-keygen # without passphrase`

-   associate a git CI user on the slave, running on a shell for the
    user ci

`git config --global user.name "CI" &&`
`git config --global user.email ci@dummyuser.ci.inria.fr`

-   Go on Gitlab, on your project select "Settings" menu, and in the
    submenu "repository", you have a "Expand" button in the "Deploy
    Keys" subsection
-   Choose a title, and copy paste the public key previously created
    (/builds/.ssh/id\_rsa.pub)
-   Click *add key*
-   Go on Jenkins and add
    git@gitlab.inria.fr:\<userGitlab\>/\<nameOfTheProjectOnGitlab\>.git
    in the field\
    *Repository URL* like in the image below:

![](/doc-ci/img/scm.png "/doc-ci/img/scm.png")

-   Do not take care about the warnings of Jenkins saying *Failed to
    connect to repository : \...*, but try the command and validate the
    key on the slave :

<code> git ls-remote -h
git@gitlab.inria.fr:<userGitlab>/<nameOfTheProjectOnGitlab>.git HEAD

Are you sure you want to continue connecting (yes/no)? yes

Warning: Permanently added 'gitlab.inria.fr,128.93.193.8' (ECDSA) to
the list of known hosts. </code>

Launch Jenkins build automatically after a commit
-------------------------------------------------

-   To launch a build automatically after pushing a commit on the
    repository, we need to set up a **hook** in Gitlab attached to our
    job of Compilation in Jenkins, To do that :

### Jenkins Config

-   Go in Jenkins , Select your Job, *Configure*, Go into the section
    *Build Triggers*
-   Check that **Build when a change is pushed to GitLab** is checked
-   Click on **Advanced**, and **Generate** : it generate a Secret token
    , copy into the Clipboard

**Note:** if your Jenkins is \> 2.129, please be aware of the following
security hardening :
<https://jenkins.io/blog/2018/07/02/new-api-token-system/>
![](/doc-ci/img/trigger.png "fig:/doc-ci/img/trigger.png")

As Gitlab will connect as anonymous to Jenkins for hooks, anonymous
requires read permission on your project. In *Manage Jenkins*,
*Configure Global Security* ( or
<https://ci.inria.fr/><nameOfTheProjectCI>/configureSecurity/ ), add
anonymous permissions to *read - overall* and *read - job* in the
permission table.

![](/doc-ci/img/AnonymousPermissionsForGitlab.png "/doc-ci/img/AnonymousPermissionsForGitlab.png")

### Gitlab Config

-   Click on ![](/doc-ci/img/dentele.png "fig:/doc-ci/img/dentele.png"),
    and Select *Integrations*
-   Fill *URL* with an url of the form
    https://ci.inria.fr/\<nameOfTheProjectCI\>/project/\<jobname\>
-   Paste your **secret Token** generated by Jenkins
-   select **Push Events** and **Merge Requests Events** and Click **Add
    Webhook**
-   you could manage to do a test with the **Test** button

Send build report to Gitlab
---------------------------

Now you want to receive a report of build (manage or failed) on the
Gitlab platform :

-   Add your sections of compilation, tests, etc\.... (cf. the faq for
    that)
-   Go into your job configuration on Jenkins, in the sub-section "Post
    Build Actions" and add **Publish build status to GitLab commit**
    into that section
-   (Optional) : you can add a building compilation badge into the
    README.md of your project with code like that

\[!\[Build
status\](https://gitlab.inria.fr/\<userGitlab\>/\<nameOfTheProjectOnGitlab\>/badges/master/build.svg)\](https://gitlab.inria.fr/\<userGitlab\>/\<nameOfTheProjectOnGitlab\>/commits/master)
