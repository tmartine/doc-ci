---
title: "BetaTest"
---
Pattern : Nom logiciel (équipe)

-   Logiciel
    -   language : ???
    -   OS : ???
    -   build : ???
    -   tests : ???
    -   URL : ???
-   Contacts : ???
-   Prêt pour la beta : oui/non

Bordeaux
========

EZTrace (SED Bordeaux)
----------------------

-   Logiciel
    -   language : C
    -   OS : Linux/BSD
    -   build : Autotools
    -   tests : Autotools/homemade
    -   URL : <http://eztrace.gforge.inria.fr/>
-   Contacts : Francois Rué
-   Prêt pour la beta : oui

Diasuite (Phoenix)
------------------

-   Logiciel
    -   langage : Java
    -   OS : Windows
    -   build : Ant
    -   tests : JUnit partiel
    -   URL : <http://diasuite.inria.fr>
-   Contacts : Damien Martin-Guillerez, Emilie Balland
-   Prêt pour la beta : \~non

Magus (Magnome) ???
-------------------

-   Logiciel
    -   langage : Perl
    -   OS : Linux
    -   build : interpreté
    -   tests : oui ???
    -   URL : \--
-   Contacts : Florian Lajus, David Sherman
-   Prêt pour la beta : \~oui

Grenoble
========

Mumps (Graal)
-------------

-   Logiciel
    -   langage : Fortran
    -   OS : Unix, Windows, Mac
    -   build : Makefiles
    -   tests : homemade
    -   URL : <http://graal.ens-lyon.fr/MUMPS/>
-   Contacts : Jean-Yves l' Excellent, Maurice Brémond
-   Prêt pour la beta : non

Siconos (Bipop)
---------------

-   Logiciel
    -   langage : Fortran, C, C++, python
    -   OS : Unix, Windows, Mac
    -   build : cmake
    -   tests : C++: cppunit, python : py.test, C homemade, all with
        CTest + CDash
    -   URL : <http://siconos.gforge.inria.fr/>
-   Contacts : Vincent Acary, Maurice Brémond
-   Prêt pour la beta : non

Lille
=====

Pharo (Rmod)
------------

-   Logiciel
    -   langage : ???
    -   OS : ???
    -   build : ???
    -   tests : ???
    -   URL : ???
-   Contacts : ???
-   Prêt pour la beta : oui/non

Sofa (Shacra)
-------------

-   Logiciel
    -   langage : ???
    -   OS : ???
    -   build : ???
    -   tests : ???
    -   URL : ???
-   Contacts : ???
-   Prêt pour la beta : oui/non

Frascati (Adam)
---------------

-   Logiciel
    -   langage : ???
    -   OS : ???
    -   build : ???
    -   tests : ???
    -   URL : ???
-   Contacts : ???
-   Prêt pour la beta : oui/non

Nancy
=====

Leopar (Sémagramme)
-------------------

-   Logiciel
    -   langage : OCaml
    -   OS : Linux
    -   build : Makefile
    -   tests : homemade
    -   URL : <http://leopar.loria.fr/>
-   Contacts : Bruno Guillaume
-   Prêt pour la beta : oui (utilise Jenkins)

CadoNFS (Caramel)
-----------------

-   Logiciel
    -   langage : C/C++
    -   OS : Linux
    -   build : CMake + Autotools
    -   tests : homemade
    -   URL : <http://cado-nfs.gforge.inria.fr/>
-   Contacts : Alexander Kruppa, Paul Zimmermann
-   Prêt pour la beta : oui

Tom (Pareo)
-----------

-   Logiciel
    -   langage : Java
    -   OS : Linux
    -   build : Ant
    -   tests : Junit
    -   URL : <http://tom.loria.fr/wiki/index.php5/Main_Page>
-   Contacts : Jean-Christophe Bach, Pierre-Etienne Moreau
-   Prêt pour la beta : oui (utilise Jenkins)

Simgrid (AlGorille)
-------------------

-   Logiciel
    -   langage : C++
    -   OS : Linux/Windows
    -   build : CMake
    -   tests : CTest
    -   URL : <http://simgrid.gforge.inria.fr/>
-   Contacts : Paul Bedaride, Martin Quinson
-   Prêt pour la beta : oui (utilise Jenkins)

Rennes
======

H2OLAB (SAGE)
-------------

-   Logiciel
    -   language : C++
    -   OS : Windows/Linux 32bits/64bits, AIX(PowerSys)
    -   Compilateurs : msvc (2005), gcc, xlc
    -   build : Visual Studio (Windows), CMake (Linux/AIX)
    -   tests : script ad-hoc .bat (lancement .exe et comparaison avec
        des résultats de référence)
    -   URL : <http://h2olab.inria.fr/>
-   Contacts : Aurélien Le Gentil, Géraldine Pichot
-   Prêt pour la beta : oui
-   workflow : ci sur branche 'experimental' avec merge auto vers
    'master' si build ok

Openvibe (VR4I)
---------------

-   Logiciel
    -   language : C++
    -   OS : Windows/Linux (Ubuntu/Fedora) 32/64 bits
    -   build : CMake
    -   tests : CTest
    -   URL : <http://openvibe.inria.fr/>
-   Contacts : Guillermo
-   Prêt pour la beta : oui

??? (LAGADIC)
-------------

-   Logiciel
    -   language : ???
    -   OS : ???
    -   build : ???
    -   tests : ???
    -   URL : ???
-   Contacts : Fabien Spindler
-   Prêt pour la beta : oui/non

??? (Triskell)
--------------

-   Logiciel
    -   language : Java
    -   OS : ???
    -   build : Maven
    -   tests : ???
    -   URL : ???
-   Contacts : Didier Vojtisek
-   Prêt pour la beta : oui/non

Rocquencourt
============

Verdandi (Clime)
----------------

-   Logiciel
    -   language : C++
    -   OS : GNU/Linux, Mac OS X, Windows
    -   build : scons
    -   tests :
    -   URL : <http://verdandi.gforge.inria.fr/>
-   Contacts : Vivien Mallet
-   Prêt pour la beta : oui/non

??? (Contraintes)
-----------------

-   Logiciel
    -   language : ???
    -   OS : ???
    -   build : ???
    -   tests : ???
    -   URL : ???
-   Contacts : Sylvain Soliman
-   Prêt pour la beta : oui/non

??? (Alpage)
------------

-   Logiciel
    -   language : Perl et C (principaux), aussi Dialog et Python
    -   OS : Red Hat, Ubuntu, Mac OS X
    -   build : script d'install Perl + autotools
    -   tests : ???
    -   URL : ???
-   Contacts : Eric De La Clergerie
-   Prêt pour la beta : oui/non

Saclay
======

ICon (InSitu)
-------------

-   Logiciel
    -   langage : Java / nombreuses librairies natives
    -   OS : Utilisé régulièrement sous Linux et OSX
    -   build : Ant, passage vers Maven en cours
    -   tests : Non
    -   URL : inputconf.sf.net
-   Contacts : Romain Primet (romain.primet@inria.fr) / Stéphane Huot
-   Prêt pour la beta : oui

Outils GridObservatory
----------------------

-   Logiciel
    -   langage : Perl
    -   OS : ???
    -   build : ???
    -   tests : ???
    -   URL : ???
-   Contacts : Julien Nauroy (Julien.Nauroy@inria.fr)
-   Prêt pour la beta : oui

OpenMEEG (Parietal)
-------------------

-   Logiciel
    -   langage : C++
    -   OS : mac os, linux 32 / 64 + windows 32 / 64
    -   build : cmake
    -   tests : ctest
    -   URL : ???
-   Contacts : Alexandre Gramfort (Alexandre.Gramfort@inria.fr)
-   Prêt pour la beta : oui?

Sophia
======

Vcore (???)
-----------

-   Logiciel
    -   langage : C++/Qt
    -   OS : ???
    -   build : cmake
    -   tests : ???
    -   URL : ???
-   Contacts : Jean-Christophe Lombardo
-   Prêt pour la beta : oui/non

DTK (SED)
---------

-   Logiciel
    -   langage : ???
    -   OS : ???
    -   build : ???
    -   tests : ???
-   Contacts : T. Kloczko, J. Wintz, N. Niclausse
-   Prêt pour la beta : oui/non
