---
title: "Tickets"
---
Que faire lorsqu'un ticket utilisateur envoie un ticket sur ci-support@inria.fr ?
-----------------------------------------------------------------------------------

-   1/ Prévenir l'utilisateur que le problème est pris en charge
-   2/ Analyser le problème
    -   Si on peut résoudre le problème de l'utilisateur directement,
        on gère le problème
    -   Si on ne peut pas résoudre le problème de l'utilisateur
        directement mais que ça ne relève pas d'un bug, on transmet la
        demande au contact local de l'équipe support
    -   Si le problème relève d'un soucis lié au portail Web, à
        l'exécution des instances Jenkins ou à l'exécution des slaves
        CloudStack
        -   Création d'un ticket sur **ci-dev@inria.fr** avec
            **ci-staff-tracking@inria.fr** en CC. Le ticket doit
            comporter tous les éléments techniques permettant de
            reproduire le problème
        -   Lorsque le problème est résolu par l'équipe de
            développement, avertir l'utilisateur de la résolution.
    -   Si le problème relève d'un soucis d'infrastructre
        -   Création d'un ticket sur **dsi-sesi.helpdesk@inria.fr**
            avec **ci-staff-tracking@inria.fr** en CC. Le ticket doit
            comporter tous les éléments techniques permettant de
            reproduire le problème
        -   Si le problème impacte tous les utilisateurs, faire un mail
            synthétique sur ci-announces@inria.fr
        -   Lorsque le problème est résolu par l'équipe de
            développement, avertir l'utilisateur de la résolution.
-   3/ Répondre à l'utilisateur

Capitalisation d'expérience
----------------------------

Lorsqu'une question liée à de la configuration de Jenkins ou CloudStack
est résolue par un membre de l'équipe support ou par un SED, il faut
ajouter une entrée dans la FAQ et éventuellement étendre la
documentation associée.
