---
title: "Interactions"
---
Pour la suite, on définit :

-   ci-dev = équipe de développement SED
-   ci-exploit-sesi = équipe d'exploitation SESI
-   ci-exploit-sed = équipe support SED, plus précisément EJ ou un
    membre de ci-exploit-sed ayant reçu une délégation
-   ci-copil = comité de pilotage

Ce document présente les interactions nécessaires entre ci-dev,
ci-exploit-sesi, ci-exploit-sed et ci-copil pour la bonne gestion de la
plateforme d'intégration continue.

Nous considérons que les délais spécifiés dans la suite du document sont
valides dans l'espace temporel suivant :

-   du lundi au vendredi (hors jour férié et RTT imposés)
-   de 9h à 12h et de 14h à 17h

Les différentes interactions entre ci-dev, ci-exploit-sesi,
ci-exploit-sed et ci-copil doivent passer soit par mails, soit par
tickets RT. Sauf cas exceptionnel, afin de pouvoir garder une trace des
échanges il ne devrait pas y avoir d'interaction téléphonique.

Évolution logicielle de la plateforme
=====================================

Régulièrement, ci-dev produit des mises à jour de la plateforme que ce
soit pour corriger des anomalies ou pour proposer des nouvelles
fonctionnalités. Lorsque ci-dev juge qu'une évolution est prête à être
déployée, une demande de déploiement est faite à ci-exploit-sed. La
demande doit préciser :

-   un tag SVN à déployer
-   un changelog
-   éventuellement les actions manuelles à effectuer

Ensuite, nous distinguons deux types d'évolutions.

Évolutions simples
------------------

Ces évolutions ne concernent que la partie WEB et leur déploiement est
automatisé selon la procédure décrite dans
<https://wiki.inria.fr/ci/Deploiement_evolution>.

Le déploiement de l'évolution se passe en deux phases, tout d'abord un
déploiement sur la plateforme de qualification, puis un déploiement sur
la plateforme de production.

### Déploiement sur la plateforme de qualification

-   ci-dev informe ci-exploit-sed via un mail sur ci-staff@inria.fr
-   ci-exploit-sed pose un ticket sur la file RT
    dsi-sesi.helpdesk@inria.fr pour demander le déploiement en
    qualification en précisant le de tag SVN associé à l'évolution. Le
    champ objet de la demande comporte \[CI-simple-qualif-update\]
-   ci-exploit-sesi effectue ce déploiement sous 2h à partir de la
    demande de ci-exploit-sed. Compte-tenu du fait qu'il s'agit d'un
    simple script à exécuter, l'opération prend moins de 5 minutes.
-   ci-exploit-sesi acquitte le déploiement sur le ticket RT posé par
    ci-exploit-sed
-   ci-exploit-sed teste le bon fonctionnement de la version mise en
    qualification. En cas de bon fonctionnement, ci-exploit-sed prévient
    ci-exploit-sesi dans le ticket RT que c'est OK et que l'on pourra
    passer ultérieurement en production. En cas de mauvais
    fonctionnement, ci-exploit-sed fait un retour à ci-dev via un ticket
    sur ci-dev@inria.fr et ci-exploit-sed prévient ci-exploit-sesi dans
    le ticket RT que ce n'est pas bon. Dans tous les cas,
    ci-exploit-sesi peut ensuite fermer le ticket.

### Déploiement sur la plateforme de production

Cette phase est conditionnée par le succès de la dernière étape du
déploiement en qualification.

-   ci-exploit-sed pose un ticket sur la file RT
    dsi-sesi.helpdesk@inria.fr pour demander la planification du
    déploiement en production de l'évolution
    -   si la demande est urgente, le champ objet de la demande commence
        par \[URGENT CI-simple-prod-update\]
    -   sinon, le champ objet de la demande commence par
        \[CI-simple-prod-update\]
-   ci-exploit-sesi acquitte sur le ticket RT dans les 2h la demande et
    propose un horaire à ci-exploit-sed (de façon à ce que
    ci-exploit-sed puisse informer les utilisateurs de l'heure de la
    mise à jour) :
    -   dans les 2h si la demande est urgente
    -   dans les 2 jours si la demande n'est pas urgente
-   ci-exploit-sed valide la proposition de ci-exploit-sesi et donne le
    tag SVN correspondant (ce tag doit être le même que celui donné pour
    le déploiement en qualification) à la version à déployer dans le
    ticket
-   ci-exploit-sesi acquitte la fin de l'exécution de l'opération sur
    le ticket posé par ci-exploit-sed
-   ci-exploit-sed valide que la mise à jour s'est bien passée. Si ça
    c'est bien passé, ci-exploit-sed prévient ci-exploit-sesi sur le
    ticket RT et ci-exploit-sesi peut fermer le ticket. Si ce n'est pas
    le cas ci-exploit-sed demande en urgence un rollback à
    ci-exploit-sesi (méthode à définir, si possible sous 1h maximum) et
    fait remonter l'incident à ci-dev via un ticket sur
    ci-dev@inria.fr.

Évolutions complexes
--------------------

Le déploiement de ces évolutions ne peuvent pas être complètement
automatisées de la façon habituelle, typiquement elles impliques des
manipulations sur la base de données ou l'annuaire LDAP.

Le déploiement de l'évolution se passe en deux phases, tout d'abord un
déploiement sur la plateforme de qualification, puis un déploiement sur
la plateforme de production.

### Déploiement sur la plateforme de qualification

-   ci-dev informe ci-exploit-sed via un mail sur ci-staff@inria.fr et
    donne estimation du temps nécessaire au déploiement de l'évolution
-   ci-exploit-sed pose un ticket sur la file RT
    dsi-sesi.helpdesk@inria.fr pour demander le déploiement en
    qualification en précisant le tag SVN associé à l'évolution ainsi
    que les opérations manuelles à exécuter. Le champ objet de la
    demande comporte \[CI-complex-qualif-update\]
-   ci-exploit-sesi effectue ce déploiement sous 4h à partir de la
    demande de ci-exploit-sed
-   ci-exploit-sesi acquitte le déploiement sur le ticket RT posé par
    ci-exploit-sed
-   ci-exploit-sed teste le bon fonctionnement de la version mise en
    qualification. En cas de bon fonctionnement, ci-exploit-sed prévient
    ci-exploit-sesi dans le ticket RT que c'est OK et que l'on pourra
    passer ultérieurement en production. En cas de mauvais
    fonctionnement, ci-exploit-sed fait un retour à ci-dev via un ticket
    sur ci-dev@inria.fr et ci-exploit-sed prévient ci-exploit-sesi dans
    le ticket RT que ce n'est pas bon. Dans tous les cas,
    ci-exploit-sesi peut ensuite fermer le ticket.

### Déploiement sur la plateforme de production

Cette phase est conditionnée par le succès de la dernière étape du
déploiement en qualification.

-   ci-exploit-sed pose un ticket sur la file RT
    dsi-sesi.helpdesk@inria.fr pour demander la planification du
    déploiement en production de l'évolution
    -   si la demande est urgente, le champ objet de la demande commence
        par \[URGENT CI-complex-prod-update\]
    -   sinon , le champ objet de la demande commence par
        \[CI-complex-prod-update\]
-   ci-exploit-sesi acquitte sur le ticket RT dans les 2h la demande et
    propose un horaire à ci-exploit-sed (de façon à ce que
    ci-exploit-sed puisse informer les utilisateurs de l'heure de la
    mise à jour) :
    -   dans les 2h si la demande est urgente
    -   dans les 2 jours si la demande n'est pas urgente
-   ci-exploit-sed valide la proposition de ci-exploit-sesi et donne le
    tag SVN (ce tag doit être le même que celui donné pour le
    déploiement en qualification) correspondant à la version à déployer
    dans le ticket, ainsi que les opérations manuelles à exécuter
-   ci-exploit-sesi acquitte l'opération sur le ticket posé par
    ci-exploit-sed
-   ci-exploit-sed valide que la mise à jour s'est bien passée. Si ça
    c'est bien passé, ci-exploit-sed prévient ci-exploit-sesi sur le
    ticket RT et ci-exploit-sesi peut fermer le ticket. Si ce n'est pas
    le cas ci-exploit-sed demande en urgence un rollback à
    ci-exploit-sesi (méthode à définir, si possible sous 1h maximum) et
    fait remonter l'incident à ci-dev via un ticket sur
    ci-dev@inria.fr.

Incidents sur la plateforme
===========================

Incidents liés à l'infrastructure
----------------------------------

### Incident observé par ci-exploit-sesi

Lorsque ci-exploit-sesi remarque un incident dans l'infrastructure qui
impacte le service :

-   ci-exploit-sesi envoie un mail à ci-staff@inria.fr immédiatement et
    met ci-staff-tracking@inria.fr en CC du ticket RT utilisé par
    ci-exploit-sesi pour la résolution du problème (peu importe quelle
    sera la file RT utilisée par ci-exploit-sesi pour résoudre le
    problème en interne) pour que ci-exploit-sed ait une vue globale de
    l'incident. ci-exploit-sesi démarre l'intervention dans les 2h.
-   après analyse, ci-exploit-sed prévient les utilisateurs via
    ci-announces@inria.fr
-   lorsque l'incident est résolu, ci-exploit-sesi informe
    ci-exploit-sed via ci-staff@inria.fr
-   C prévient les utilisateurs de la fin de l'incident via
    ci-announces@inria.fr

### Incident observé par ci-exploit-sed

-   ci-exploit-sed reporte l'incident à ci-exploit-sesi via un ticket
    sur dsi-sesi.helpdesk@inria.fr avec le maximum d'informations qui
    illustrent ou permettent de reproduire le problème,
    ci-staff-tracking@inria.fr est mis en CC du ticket
-   ci-exploit-sesi démarre l'analyse du problème dans les 2h et donne
    à ci-exploit-sed une idée du délais nécessaire à la résolution du
    problème
-   ci-exploit-sed informe les utilisateurs via ci-announces@inria.fr
-   à la résolution du problème, ci-exploit-sesi informe ci-exploit-sed
    via le ticket posé
-   ci-exploit-sed prévient les utilisateurs de la fin de l'incident
    via ci-announces@inria.fr

Incidents liés au logiciel développé
------------------------------------

-   ci-exploit-sed reporte l'incident à ci-dev via un ticket sur
    ci-dev@inria.fr avec le maximum d'informations qui illustrent ou
    permettent de reproduire le problème, ci-staff-tracking@inria.fr
    est mis en CC du ticket
-   ci-dev démarre l'analyse du problème dans les 2 jours et donne à
    ci-exploit-sed une idée du délais nécessaire à la résolution du
    problème (ce qui se résume au temps nécessaire avant qu'une
    nouvelle évolution puisse être déployée sur la plateforme de
    qualification)
-   si le problème est très gênant, ci-exploit-sed informe les
    utilisateurs via ci-announces@inria.fr
-   Une fois le problème corrigé, ci-dev propose une nouvelle évolution
    à ci-exploit-sed selon la procédure décrite précédemment

Arrêt programmé de la plateforme
================================

Pour diverses raisons, ci-exploit-sesi peut-être amené à éteindre la
plateforme, ce qui rendrait le service inaccessible aux utilisateurs.

Arrêt impactant la plateforme de qualification
----------------------------------------------

-   ci-exploit-sesi prévient ci-exploit-sed au moins 4h avant via un
    mail sur ci-staff@inria.fr
-   ci-exploit-sed valide dans les meilleurs délais en répondant au mail
    envoyé
-   ci-exploit-sesi lance l'opération et prévient de sa fin en le
    signalant en réponse du mail initial

Arrêt impactant la plateforme de production
-------------------------------------------

Ce type d'arrêt doit rester rare (pas plus de 1 jour par an d'arrêts
cumulé sauf cas de force majeure). Si l'arrêt demande plus d'une
journée, ci-copil doit valider.

-   ci-exploit-sesi prévient ci-exploit-sed au moins 2 semaines avant
    via un mail sur ci-staff@inria.fr et donne une idée du temps requis
    pour l'arrêt
-   ci-exploit-sed valide dans les 4h en répondant au mail envoyé
-   ci-exploit-sed prévient les utilisateurs via ci-announces@inria.fr
    avec les dates précises de l'interruption de service
-   ci-exploit-sesi effectue la maintenance, redémarre le service et
    vérifie que tout est reparti, puis prévient ci-exploit-sed
-   ci-exploit-sed prévient les utilisateurs de la fin de la maintenance
    via ci-announces@inria.fr
