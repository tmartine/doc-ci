---
title: "Event 20141125 storage"
---
Storage infrastructure maintenance (25/11/14)
---------------------------------------------

A maintenance session is planned on the storage infrastructure on
2014/11/25 from 5pm to 11pm.

-   2014-11-26 10:00\> The service is still not up. IT team has been
    contacted to get more information.
-   2014-11-26 11:45\> The service is up.
