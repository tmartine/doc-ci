---
title: "Cr20140116"
---
Audio équipe support CI, 16/01/14

Présents

-   Bruno
-   Anthony
-   David
-   Marc
-   Maurice
-   Laurent
-   Emmanuel

Absents

-   Thibaud

Arrivée de Marc
===============

Marc Fuentes prend le relais de Damien dans l'équipe support.

Infos
=====

-   mac : pas avant second semestre
-   CS4 : déploiement fin janvier en qualif, et prod une semaine plus
    tard
-   Marc a généralisé la transformation des sorties de tests CTest en
    JUnit : réécriture du script shell en CMake. Il a modifié la FAQ :
    <https://wiki.inria.fr/ciportal/FAQ#Display_CTest_results_in_Jenkins>

Retours
=======

-   anthony: problème d'upload d'ISO
    -   <https://tickets.inria.fr/Ticket/Display.html?id=175047>
    -   <https://tickets.inria.fr/Ticket/Display.html?id=174825>
-   laurent: freeze windows -\> à voir si ça reproduit avec CS4
-   laurent: doc administrateur

<https://wiki.inria.fr/ciportal/SupportTeam>

EJ doit documenter les différents accès admin, regarder le mail de
laurent sur ci-staff du 14//11/13

Sondage
=======

Nous souhaitons faire un sondage auprès des utilisateurs pour faire le
point sur les cas d'usage et l'adéquation de l'offre actuelle, ainsi
que sur le niveau de satisfaction des utilisateurs.

-   viser un envoi au 15/2
-   construction collaborative (EJ : création pad/wiki et diffusion sur
    la liste)

Fonctionnalités admins nécessaires
==================================

État actuel
-----------

-   portail : ajouter/supprimer des projets/utilisateurs, voir tous les
    projets
-   jenkins : accès au système de fichier
-   cloudstack : contrôle sur toutes les machines virtuelles (en se
    logguant sur le domaine ci et pas ci/projet)

Besoin
------

-   jenkins : accès administrateur aux projets

Divers
======

-   BUGS
    -   start/stop d'une instance qui serait désynchronisée depuis le
        portail
    -   bruno: remonter à sesi que lorsque l'on recherche les vm d'un
        projets dans cloudstack en étant loggué sur le domaine ci
    -   david,marc,maurice,thibaud?: connection impossible sur le
        domaine CI
    -   bruno: étendre la doc pour dire qu'il faut préfixer les noms de
        machines par projet- quand on crée directement depuis cloudstack
-   rappel de l'existance d'un salon jabber ci
