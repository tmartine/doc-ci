---
title: "Poll"
---
Tentative de sondage à envoyer aux utilisateurs de CI

### Continuous integration practices

-   Languages used in your project
    -   C
    -   C++
    -   Erlang
    -   Fortran
    -   Java
    -   Haskell
    -   Scala
    -   Ruby
    -   OCaml
    -   Perl
    -   PHP
    -   Python
    -   Other ()

<!-- -->

-   Source code management
    -   Inria Forge
    -   GitHub
    -   Gitorious
    -   SourceForge
    -   Bitbucket
    -   Team managed SCM ()

<!-- -->

-   Build (y/n)
    -   Trigger
        -   Manual
        -   After a commit
        -   Every X minutes
    -   Build framework
        -   Makefile
        -   Cmake
        -   Autotools
        -   Ant
        -   Maven
    -   Compiler
        -   Clang
        -   GCC/G++/Fortran
        -   Intel Compiler
        -   OpenJDK
        -   Oracle JDK
        -   PGI Compiler
        -   Other ()

<!-- -->

-   Test (y/n)
    -   Trigger
        -   Manual
        -   After a commit
        -   Every X minutes
    -   Test level
        -   Unit testing
        -   Integration testing
        -   System testing
    -   Test framework
        -   Junit
        -   CTest
        -   Doctest
        -   Test::Unit Ruby
        -   Other ()
    -   Memory leak (y/n)
        -   Tool
            -   Allinea DDT
            -   Insure++
            -   TotalView
            -   Valgrind
            -   Other ()

<!-- -->

-   Documentation generation (y/n)
    -   Framework
        -   Doxygen
        -   Javadoc
        -   Other ()

<!-- -->

-   Package generation (y/n)
    -   Linux packages
        -   Debian/Ubuntu
        -   CentOS/Fedora/Redhat
        -   Other ()
    -   Windows Packages

### Your CI@Inria usage

-   OS usage
    -   Linux
        -   Archlinux
        -   CentOS
        -   Debian
        -   Fedora
        -   RedHat
        -   Ubuntu
        -   OpenSUSE
    -   \*BSD
        -   FreeBSD
        -   NetBSD
        -   OpenBSD
    -   MacOS
    -   Windows
        -   Windows XP
        -   Windows 7
        -   Windows 8

<!-- -->

-   Access to external resources (y/n)
    -   Slaves (y/n)
        -   Specific hardware (y/n)
            -   Cluster
            -   GPU
            -   FPGA
            -   MIC
            -   Other
        -   Resource with reservation tool (y/n)
            -   OAR
            -   SGE
            -   Other
        -   Cloud (y/n)
            -   Amazon EC2
            -   Rackspace
            -   Other
    -   Large storage facility (y/n)

### Your opinion of CI@Inria

-   Overall satisfaction
    -   Very Satisfied
    -   Somewhat Satisfied
    -   Neither Satisfied Nor Dissatisfied
    -   Somewhat Dissatisfied
    -   Very Dissatisfied

<!-- -->

-   Downtime or partial service unavailability frequency
    -   I don't care
    -   Acceptable
    -   Annoying
    -   Unacceptable

<!-- -->

-   Communication about incident (mails ci-announces@inria.fr +
    <https://wiki.inria.fr/ciportal/Ciportal:Current_events>)
    -   Overall quality
        -   Perfect
        -   Good
        -   Average
        -   Bad
    -   Technical level
        -   Good
        -   Not enough details
        -   Too much details

<!-- -->

-   Help when asking to support team (ci-support@inria.fr\]
    -   Overall quality
        -   Perfect
        -   Good
        -   Average
        -   Bad
    -   Reactivity
        -   Good
        -   Could be improved
        -   Bad

<!-- -->

-   Web portal (https://ci.inria.fr/) ease of use
    -   Perfect
    -   Good
    -   Average
    -   Bad

<!-- -->

-   Documentation (https://wiki.inria.fr/ciportal/Main\_Page) quality
    -   Perfect
    -   Good
    -   Average
    -   Bad

<!-- -->

-   Does CI@Inria have improved the quality of your software (y/n)

<!-- -->

-   Would you recommend the use of CI@Inria to you colleagues (y/n)

<!-- -->

-   Do you want to add something? ()
