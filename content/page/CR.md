---
title: "CR"
---
Comptes-rendus de réunion de l'équipe support
==============================================

-   [28 juin 2012 (audio)](../cr20120628 "wikilink")
-   [12 juillet 2012 (audio)](../cr20120712 "wikilink")
-   [14 août 2012 (audio)](../cr20120814 "wikilink")
-   [13 septembre 2012 (audio)](../cr20120913 "wikilink")
-   [8 octobre 2012 (visu)](../cr20121008 "wikilink")
-   [11 octobre 2012 (audio)](../cr20121011 "wikilink")
-   [14 décembre 2012 (audio)](../cr20121214 "wikilink")
-   [8 janvier 2013 (visio)](../cr20130108 "wikilink")
-   [18 février 2013 (audio)](../cr20130218 "wikilink")
-   [28 mai 2013 (audio)](../cr20130528 "wikilink")
-   [17 septembre 2013 (audio)](../cr20130917 "wikilink")
-   [16 janvier 2014 (audio)](../cr20140116 "wikilink")
-   [2 octobre 2014 (audio)](../cr20141002 "wikilink")
-   [16 avril 2015 (audio)](../cr20150416 "wikilink")
-   [21 juin 2018 - Offre GPU pour CI
    (audio)](../cr20180621gpu "wikilink")

Comptes-rendus de l'équipe de dev
==================================

<https://wiki.inria.fr/ci/CompteRendus>
