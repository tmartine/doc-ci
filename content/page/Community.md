---
title: "Community"
---
![](/doc-ci/img/Community.png "/doc-ci/img/Community.png")

Users are welcomed to share their knowledge related to:

-   general concepts and good practices about continuous integration
-   advanced usage of Jenkins
-   creation of slave template with non-featured operating systems, see
    [this page](../create_community_slaves "wikilink") for more
    information.
-   etc

A good idea is to:

1.  subscribe to the dedicated mailing-list:
    [ci-community](https://sympa.inria.fr/sympa/info/ci-community)
2.  send a message to *ci-community@inria.fr* when you want to share a
    useful experience

If you want to write a specific documentation, you can create a page in
this Wiki linked from the [following page](../user_pages "wikilink").

Community templates
===================

Caution
-------

When creating a qcow2 template, ensure that it follows the qcow2 v2
format, otherwise it won't work on CI. You can generate such qcow2 file
using the compat option of qemu-img like:

`$>qemu-img create -f qcow2 -o compat=0.10 mytemplate.qcow2 10G`

Linux
-----

### Fedora 17

-   Author: Nicolas Niclausse

### Debian Squeeze 6.0.6

-   Author: Brice Goglin
-   Notes: sshd and openjdk are installed and configured with usual
    default passwords for root and ci

### Debian Wheezy 7.0.0 64bits

-   Author: Emmanuel Jeanvoine
-   Notes: sshd and openjdk are installed and configured with usual
    default passwords for root and ci

\*BSD
-----

### OpenBSD 5.2 and 5.3

-   Author: Emmanuel Thomé, Paul Zimmermann

### FreeBSD 9.1

-   Author: Brice Goglin
-   Notes: sshd and openjdk are installed and configured with usual
    default passwords for root and ci

### NetBSD 6.0.1

-   Author: Brice Goglin
-   Notes: sshd and openjdk are installed and configured with usual
    default passwords for root and ci
