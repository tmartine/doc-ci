---
title: "PRA"
---
Cette page décrit une procédure de vérification du fonctionnement de la
plateforme d'intégration continue après un incident. Les étapes
ci-dessous doivent être exécutées en séquence et en cas d'incident sur
une étape, il faut remonter le problème auprès des équipes infra et dev
au plus vite

-   Création d'un projet
    -   Shortname : PRA
    -   Fullname : test de plateforme d'intégration continue
    -   Description : plan de reprise d'activité
    -   Public : private
    -   Software : Jenkins

<!-- -->

-   Configuration de Jenkins
    -   clic sur l'onglet Dashboard
    -   clic sur le bouton Jenkins du projet PRA
    -   login dans Jenkins
    -   Clic sur manage Jenkins -\> Manage PLugins -\> Available
        -   rechercher et installer Jenkins GIT plugin
        -   redémarrer l'instance Jenkins
    -   New Job
        -   Job Name : PRA
        -   Build a free-style software project
        -   Source Code Management : Git
            -   Repository URL :
                <git://scm.gforge.inria.fr/simgrid/simgrid.git>
