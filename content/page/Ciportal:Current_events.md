---
title: "Ciportal:Current events"
---
Current events
--------------

-   No current event

Next events
-----------

-   No planned event

Past events
-----------

**2015/2/10 - 2015/2/..**

-   CloudStack upgrade. More details
    [here](../event_20150211_cs "wikilink").

**2014/11/25 - 2014/11/25**

-   Storage infrastructure maintenance. More details
    [here](../event_20141125_storage "wikilink")

**2014/04/15 - 2014/05/05**

-   Primary storage upgrade. More details
    [here](../event_20140415_cs "wikilink").

**2014/03/03 - 2014/03/09**

-   Unable to create a new VM or to start a stopped one in CloudStack.
    More details [here](../event_20140303_cs "wikilink").

**2014/02/04 - 2014/02/14**

-   CloudStack issue related to the future storage update. More details
    [here](../event_20140204_cs "wikilink").

**2014/02/13 - 2014/02/13**

-   CloudStack upgrade. More details
    [here](../event_20140213_cs "wikilink").

**2013/10/30 - 2013/10/31**

-   CloudStack issue related to unlucky maintenance. More details
    [here](../event_20131030_cs "wikilink").

'''2013/10/12 - 2013/10/30 - '''

-   CloudStack issue related to storage saturation. More details
    [here](../event_20131016_cs "wikilink").

**2013/09/12 - 2013/09/20**

-   Some VM are unreachable.

**2013/06/03 - 2013/06/10**

-   Cloudstack is partially down because of a network incident. Many
    virtual machine are currently unreachable. This is the cause of the
    error messages: *Error \#500 error :Empty or too short HTTP message*
