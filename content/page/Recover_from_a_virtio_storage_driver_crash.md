---
title: "Recover from a virtio storage driver crash"
---
Use case
--------

This procedure applies to Windows VMs crashing because of an invalid
Virtio Storage driver. The boot triggers a BSOD related to
**viostor.sys**.

Solution
--------

Remove the *viostor.sys* driver manually.

Procedure
---------

1\. boot windows in the **Startup Repair** mode

![](/doc-ci/res/Virtio "/doc-ci/res/Virtio")

2\. cancel the automatic repair (it will fail anyway)

3\. in the final screen, select **View advanced options for system
recovery and support**

![](/doc-ci/res/Virtio "/doc-ci/res/Virtio")

4\. log in as user 'ci'

5\. select **command prompt**

6\. find the windows directory (it should be available in drive **D:**)
and go to the **system32** directory

`D:`\
`cd \windows\system32`

7\. find all occurrences of **viostor.sys**

`dir /s /b viostor.sys`

8\. remove them

`del system32\viostor.sys`\
`del xxxxxxxxxxxxxxx\xxxxxxxxx\viostor.sys`\
`...`

9\. reboot the VM

![](/doc-ci/res/Virtio "/doc-ci/res/Virtio")
