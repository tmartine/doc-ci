---
title: "Event 20150211 cs"
---
CloudStack upgrade (11/02/2014)
-------------------------------

### Facts

-   CloudStack will be upgraded to the 4.4.2 version.

### Actions

-   2015-02-10 17:00\> Beginning of the maintenance
-   2015-02-11 22:00\> After of a lot of work to perform the upgrade,
    the system VM have been restarted, but user VM still cannot be
    restarted.
-   2015-02-12 09:00\> IT team is still working on the issue.
-   2015-02-12 17:00\> Service is partially back. VM have to be
    restarted manually by users according to their needs. New VM still
    cannot be created.
-   2015-02-17 09:00\> Work in progress to solve the VM creation issue.
-   2015-02-17 22:00\> All the VM restarted by users have been shut down
    unexpectedly. The current investigation seems to incriminate a
    problem when copying data from secondary storage to primary storage.
-   2015-02-18 12:00\> The whole service has to be shut down again.
-   2015-02-18 18:00\> CS has been downgraded to the 4.2 version. A
    stable state has been reached.
-   2015-02-19 09:00\> IT team retries to perform the upgrade to 4.4.2
    version.
-   2015-02-23 12:00\> CS has been successfuly upgraded.
