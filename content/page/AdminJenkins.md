---
title: "AdminJenkins"
---
Comment intervenir directement sur le fichier config.xml d'un Jenkins
planté ?

Repérer le serveur concerné
---------------------------

Sur le portail, lorsque vous listez les projets, vous pouvez voir sur
quel serveur le jenkins correspondant est hébergé
(ci-jenkins\*.inria.fr).

Connexion au serveur
--------------------

Pour vous connectez dessus, il faut rebondir par sesi-ssh.inria.fr. ex:

`$ ssh -oProxyCommand='ssh -W ci-jenkins5.inria.fr:22 sesi-ssh.inria.fr' ci-jenkins5.inria.fr`

ou encore

`$ ssh -A <nom d'utilisateur LDAP>@sesi-ssh.inria.fr`

puis une fois sur sesi-ssh:

`$ ssh ci-jenkins5.inria.fr`

Accès au projet
---------------

Ensuite les choses intéressantes se passent dans /net/www/ci/PROJET. Le
truc c'est qu'avec notre utilisateur normal, on ne peut pas accéder au
répertoire intéressant. Il faut donc prendre l'identité de
l'utilisateur qui fait tourner les Jenkins (vous trouverez le nom
d'utilisateur en faisant un ls -l dans /net/www/ci) en faisant (par
exemple pour le projet simgrid) :

`$ sudo su - jenk_simgrid`\
`$ bash`

Accès à CloudStack
------------------

Des fois, un utilisateur ne peut se connecter à CloudStack:

1.  Vérifiez d'abord qu'il est bien admin sur le portail CI du projet
2.  Si il est Admin et qu'il ne peut cependant pas se connecter, allez
    sur le domaine CI, cliquer sur l'onglet «Accounts»\

selectionner le projet, cliquer sur «QuickView» puis sur «Enable User»

Recherche et suppression des processus
--------------------------------------

Pour trouver les processus actifs:

`$ ps x`

     PID TTY      STAT   TIME COMMAND
    8547 pts/0    S      0:00 su - jenk_bonsai
    8548 pts/0    S      0:00 -su
    10300 ?        S      0:00 /usr/bin/daemon --name=bonsai --inherit --env=JENKINS_HOME=/net/www/ci/bonsai --output=/var/log/ci/bonsai.log --pidfile=/var/run/bonsai/bons
    10301 ?        Sl     0:33 /usr/bin/java -jar /net/www/ci/bonsai/jenkins.war --webroot=/net/www/ci/bonsai/.jenkins/war --httpPort=7015 --ajp13Port=-1 --httpsPort=-1 --
    11041 pts/0    R+     0:00 ps x

Vous pouvez avoir 2 ou 4 lignes, suivant que la qualif est lancée ou
pas.

Suppression des processus `/usr/bin/daemon` et `/usr/bin/java` :

`$ kill -9 10300 10301`

Le redémarrage des processus doit se faire par l'interface
d'administration jenkins du projet : <http://ci.inria.fr>

Problème d'accés au client Jenkins
-----------------------------------

Si l'utilisateur ne réussit plus à se connecter au client Jenkins
après, par exemple, avoir essayé de rendre public les résultats de
compilation de Jenkins. Il faut éditer le `config.xml` et supprimer les
2 lignes suivantes

` `<managerDN>`my_username`</managerDN>\
` `<managerPassword>`caVSGEy5X7J=`</managerPassword>

et redémarrer l'instance Jenkins.

Édition du config.xml
---------------------

Le fichier config.xml est généralement celui à modifier.

Notez qu'il existe un config.xml.bak, c'est le premier truc à essayer
avant de réellement devoir mettre les mains dans le cambouis.

Accès aux logs Jenkins du projet
--------------------------------

Lorsque l'on est connecté au serveur (pas au projet), les fichiers de
log Jenkins sont générés par journald et accessibles via journalctl.

Quelques extractions utiles :

`* démarrages du serveur : `\
`   `<code>`sudo journalctl -u jenk_`<projet>`[-qualif].service`\
`         /Running  # comme dans un vi classique`</code>\
`* démarrages du serveur réussis :`\
`   `<code>`sudo journalctl -u jenk_`<projet>`[-qualif].service`\
`         /hudson.WebAppMain  # comme dans un vi classique`</code>

Identifier la saturation de la mémoire
--------------------------------------

Pour savoir si des processus Jenkins se sont plantés, car la mémoire
était saturée (évènement \`oom-killer\`), utilisez la commande suivante
pour rechercher les *out of memory* :
`grep oom-killer /var/log/messages`

La commande `dmesg` peut aussi être utile pour identifier les problèmes
au niveau du kernel.

Contacter le support SESI
-------------------------

En cas de problème sortant des limites du support SED à CI (accès
superuser, infrastructure, etc.), merci de contacter SESI en mettant un
ticket sur : <https://support.inria.fr/> dans la file
`dsi-sesi.helpdesk-PRC`.

Récupérer une version antérieure du projet
------------------------------------------

Il y a une sauvegarde incrémentale par snapshot du contenu des projets
Jenkins dans le dossier : `/auto/ci/.snapshot/`

Problème de démarrage Jenkins à cause d'un script d'Init Groovy
-----------------------------------------------------------------

Lorsque Jenkins ne veut pas démarrer, cela peut être du à une erreur
dans un script Groovy d'initialisation. Pour voir l'erreur :

`$ ssh -A <nom d'utilisateur LDAP>@sesi-ssh.inria.fr`\
`$ ssh ci-jenkins3.inria.fr`\
`$ sudo journalctl -u jenk_openvibe.service`

Par exemple :

`Sep 08 09:45:26 ci-jenkins3 java[20613]: SEVERE: Failed GroovyInitScript.init`\
`Sep 08 09:45:26 ci-jenkins3 java[20613]: java.lang.Error: java.lang.reflect.InvocationTargetException`\
`Sep 08 09:45:26 ci-jenkins3 java[20613]:         at ...`\
`Sep 08 09:45:26 ci-jenkins3 java[20613]: Caused by: java.lang.reflect.InvocationTargetException`\
`Sep 08 09:45:26 ci-jenkins3 java[20613]:         at ... 8 more`\
`Sep 08 09:45:26 ci-jenkins3 java[20613]: Caused by: groovy.lang.MissingMethodException: No signature of method: hudson.model.UpdateCenter.updateAllSites`\
`Sep 08 09:45:26 ci-jenkins3 java[20613]:         at org.codehaus.groovy.runtime.ScriptBytecodeAdapter.unwrap(ScriptBytecodeAdapter.java:55)`\
`Sep 08 09:45:26 ci-jenkins3 java[20613]:         at org.codehaus.groovy.runtime.callsite.PojoMetaClassSite.call(PojoMetaClassSite.java:46)`

Des scripts Init Groovy sont déployés automatiquement par puppet sur
tous les projets en Jenkins v2 et échouent sur des Jenkins v1.

Puppet ne déploie ces scripts que si le fichier jenkins1\_project.txt
n'est pas présent à la racine du projet.

Script /usr/local/sbin/check-jenkins-configuration.py
-----------------------------------------------------

Si ce script renvoie une erreur, Jenkins est redémarré. Recopier ce
script:

`$ cp /usr/local/sbin/check-jenkins-configuration.py .`

Commenter la ligne de redémarrage:

`$ vim check-jenkins-configuration.py`\
`   #call(['/usr/sbin/service', 'jenk_' + project_name, 'restart'])`

Relancer le script sur le fichier de config :

`$ ./check-jenkins-configuration.py config.xml `<nom du projet>

Vérifier qu'il n'y a pas d'erreur.

Volumétrie d'un projet
-----------------------

`$ ssh -A <nom d'utilisateur LDAP>@sesi-ssh.inria.fr`\
`$ ssh ci`\
`$ cd /var/log/ci`

Les fichiers volumetry.YYYY-MM-DD.log sont accessibles

Problème de connexion à un slave
--------------------------------

### No entry currently exists in the Known Hosts file for this host

#### Pour les admins

`  Warning: no key algorithms provided; JENKINS-42959 disabled`\
`  [02/16/18 10:36:09] [SSH] Opening SSH connection to parmmg-mavericks:22.`\
`  [02/16/18 10:36:09] [SSH] WARNING: No entry currently exists in the Known Hosts file for this host. Connections will be denied until this new host and its associated key is added to the Known Hosts file.`\
`  Key exchange was not finished, connection is closed.`

Pour résoudre ce problème, seuls les admins peuvent le corriger en
faisant :

`$ ssh -A <nom d'utilisateur LDAP>@sesi-ssh.inria.fr`\
`$ ssh ci-jenkins6.inria.fr`\
`$ sudo su - jenk_parmmg`\
`$ bash`\
`$ ssh ci@parmmg-mavericks`\
`  The authenticity of host 'parmmg-mavericks (172.21.15.46)' can't be established.`\
`  RSA key fingerprint is SHA256:2Ab8txuYa0D8q2rLbl92gHQddK1jwdx4QT4iIYoeZgU.`\
`  Are you sure you want to continue connecting (yes/no)? yes`

`  Warning: Permanently added 'parmmg-mavericks,172.21.15.46' (RSA) to the list of known hosts.`\
`  Last login: Fri Feb 16 11:38:16 2018 from 172.21.1.7`

Il semblerait que ce problème arrive lorsque les utilisateurs upgradent
leur Jenkins depuis Jenkins plutôt que depuis le portail ci.inria.fr

#### Pour les utilisateurs

Dans le champs "Host Key Verification Strategy", on peut mettre "Non
verifying Verification Strategy", et la connection se fait.

### cloud-set-guest-sshkey.in

Le fichier
![](/doc-ci/res/cloud-set-guest-sshkey.in "fig:/doc-ci/res/cloud-set-guest-sshkey.in")

cf.
<https://gitlab.inria.fr/inria-ci/ci-service/blob/master/worker-templates/cloud-set-guest-sshkey.in>

Voir aussi Jenkins2 upgrade
---------------------------

cf
[../https://wiki.inria.fr/ci/jenkins2upgrade](../https://wiki.inria.fr/ci/jenkins2upgrade "wikilink")

Connexion au LDAP CI
--------------------

Pour vous connectez dessus, il faut rebondir par sesi-ssh.inria.fr.

`$ ssh -A <nom d'utilisateur LDAP>@sesi-ssh.inria.fr`

puis une fois sur sesi-ssh:

`$ ssh ci-ldap.inria.fr`

Pour voir si le LDAP n'est pas plein :

`$ df -hT`

Message Of The Day
------------------

La distribution Ubuntu nécessite d'utiliser un script
![](/doc-ci/res/30-security-warning.sh "fig:/doc-ci/res/30-security-warning.sh")
dans /etc/update-motd.d/

Pour les autres, voire dans le fichier /etc/motd.

Cloud-init - En cours de développement
--------------------------------------

Les dernières versions des templates utilisent le paquet cloud-init
plutôt que le script cloud-set-guest-sshkey.in

Le fichier
![](/doc-ci/res/91_cloudstack.cfg "fig:/doc-ci/res/91_cloudstack.cfg")
est copié dans /etc/cloud/cloud.cfg.d/
