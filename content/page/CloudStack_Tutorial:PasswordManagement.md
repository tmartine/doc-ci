---
title: "CloudStack Tutorial:PasswordManagement"
---
This page explains how to add password reset feature to virtual machines
(useful for template creation). The password reset feature, if enabled
in Template, will reset the password at instance generation.

**Note:** This is taken directly from the CloudStack manual. It has not
been tested

To activate this in a template, you need to install extra script in your
VM:

-   For windows, download the installer
    [CloudInstanceManager.msi](http://sourceforge.net/projects/cloudstack/files/Password%20Management%20Scripts/CloudInstanceManager.msi/download)
    and run it in the Windows VM.
-   For Linux, download the script
    [cloud-set-guest-password](http://cloudstack.org/dl/cloud-set-guest-password)
    into */etc/init.d* (or */etc/rc.d/init.d*), make it executable by
    *chmod +x /etc/init.d/cloud-set-guest-password* and do:
    -   If you are in Fedora, CentOS, RHEL, or Debian, run *chkconfig
        \--add cloud-set-guest-password*.
    -   If you are in Ubuntu: first if you are running version 11.04,
        create a directory */var/lib/dhcp3*, then run *sudo update-rc.d
        cloud-set-guest-password defaults 98*. You need to have
        *mkpasswd* installed (*sudo apt-get install whois* or *sudo
        apt-get install mkpasswd* depending on your Ubuntu version.
