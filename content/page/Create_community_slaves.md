---
title: "Create community slaves"
---
The purpose of this page is to provide advices to create CloudStack
linux templates ready to use for others as Community Templates.

The page is divided in 3 parts,

-   what seems to be common to all linux Systems
-   what is specific to Debian Systems
-   what is specific to RedHat Systems

Previous steps
==============

This document is intendend to be used once the base system is installed
via iso. This part should be available somewhere else\... If you need to
create a user during system installation, one can use a user named
template-user. Everything below this line should be run as root

Common
======

-   Initialize root password (set 'password' as root password)

` passwd`

-   Initialize ci account (default password is 'ci')

` useradd -d /builds -m -p '$6$fasToP1b$X09p76wvP5L3rvVYQ.UAFghykr/Kn.EtuXyGT5wajifBq0hDNXDxZaJtzk2cUz/1gwstm.moka2/I9fwtp2gG1' -s /bin/bash ci`\
` mkdir ~ci/.ssh`\
` touch ~ci/.ssh/authorized_keys`\
` chown -R ci:ci ~ci/.ssh`\
` chmod 700 ~ci/.ssh`\
` chmod 600 ~ci/.ssh/authorized_keys`

-   Create group admin and add ci user to this group

` groupadd admin`\
` gpasswd -a ci admin`

Debian
======

-   Update the system

` apt-get update && apt-get dist-upgrade`

-   Install the common libraries and tools to the template

` apt-get -y install openjdk-6-jre openjdk-6-jdk gcc g++ make gfortran cmake python-distutils-extra rubygems ruby-rvm subversion git-core python-numpy python-scipy python-matplotlib ipython mercurial bzr openmpi1.5-bin vim emacs scons ocaml`

` apt-get -y install whois`

-   Get the scripts to work with CloudStack Integration (set ssh key on
    instance create and allow dynamically modifying password)

` wget --no-check-certificate `[`https://ci-cloud.inria.fr/cloud-set-guest-sshkey.in`](https://ci-cloud.inria.fr/cloud-set-guest-sshkey.in)` -O cloud-set-guest-sshkey.in`\
` wget --no-check-certificate `[`https://ci-cloud.inria.fr/cloud-set-guest-password`](https://ci-cloud.inria.fr/cloud-set-guest-password)` -O cloud-set-guest-password`

` cp cloud-set-guest-password /etc/init.d/`\
` chmod +x /etc/init.d/cloud-set-guest-password `\
` update-rc.d cloud-set-guest-password defaults 98`

` cp cloud-set-guest-sshkey.in /etc/init.d/`\
` chmod +x /etc/init.d/cloud-set-guest-sshkey.in `\
` update-rc.d cloud-set-guest-sshkey.in defaults 98`

-   Clean the system

` userdel -f -r template-user `\
` echo '' > /etc/hostname`

RedHat
======

-   Add the admin group to sudoers

` echo '%admin ALL=(ALL) ALL' >> /etc/sudoers`

-   Install common libraries and tools

` rpm -ivh `[`http://fr2.rpmfind.net/linux/epel/6/i386/epel-release-6-7.noarch.rpm`](http://fr2.rpmfind.net/linux/epel/6/i386/epel-release-6-7.noarch.rpm)\
` yum -y install java-1.6.0-openjdk-devel gcc gcc-c++ gcc-gfortran make cmake rubygems subversion git numpy scipy python-matplotlib ipython mercurial bzr openmpi vim-enhanced emacs-nox scons ocaml`

-   Install rvm

` yum install -y gcc-c++ patch readline readline-devel zlib zlib-devel libyaml-devel libffi-devel openssl-devel openssl make bzip2 autoconf automake libtool bison iconv-devel`\
` curl -L get.rvm.io | bash -s stable`

-   Get the scripts to work with CloudStack Integration (set ssh key on
    instance create and allow dynamically modifying password)

` wget --no-check-certificate `[`https://ci-cloud.inria.fr/cloud-set-guest-sshkey.in`](https://ci-cloud.inria.fr/cloud-set-guest-sshkey.in)` -O cloud-set-guest-sshkey.in`\
` wget --no-check-certificate `[`https://ci-cloud.inria.fr/cloud-set-guest-password`](https://ci-cloud.inria.fr/cloud-set-guest-password)` -O cloud-set-guest-password`

` cp cloud-set-guest-password /etc/init.d/`\
` chmod +x /etc/init.d/cloud-set-guest-password `\
` chkconfig --add cloud-set-guest-password `

` cp cloud-set-guest-sshkey.in /etc/init.d/`\
` chmod +x /etc/init.d/cloud-set-guest-sshkey.in `\
` chkconfig --add cloud-set-guest-sshkey.in`

-   Clean the system

` echo '' > /etc/udev/rules.d/70-persistent-net.rules `

` cat <`<EOF >` /etc/sysconfig/network-scripts/ifcfg-eth0`\
` DEVICE="eth0"`\
` BOOTPROTO="dhcp"`\
` IPV6INIT="yes"`\
` IPV6_AUTOCONF="yes"`\
` NM_CONTROLLED="yes"`\
` ONBOOT="yes"`\
` TYPE="Ethernet"`\
` EOF`

` sed 's/enforcing/permissive/g' -i /etc/selinux/config`\
` sed -i 's/HOSTNAME=.*//' /etc/sysconfig/network`

Common
======

Finally clean the root user bash history

` echo > ~/.bash_history`
